package com.lfp.ssh.impl;

import java.io.IOException;
import java.security.KeyPair;
import java.util.Objects;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;

import com.lfp.joe.utils.Utils;
import com.lfp.ssh.impl.client.SSHClientContext;
import com.lfp.ssh.impl.client.SSHClientOptions;

import ch.qos.logback.classic.Level;

public class SSHs {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static SSHClientContext clientConnect(SSHClientOptions sshClientOptions) throws IOException {
		Objects.requireNonNull(sshClientOptions);
		SshClient sshClient = null;
		ClientSession clientSession = null;
		try {
			sshClient = SshClient.setUpDefaultClient();
			if (sshClientOptions.getSshClientConfiguration() != null)
				sshClientOptions.getSshClientConfiguration().accept(sshClient);
			sshClient.start();
			var connect = sshClient.connect(sshClientOptions.getUsername(), sshClientOptions.getHostname(),
					sshClientOptions.getPort());
			var completed = connect.await(sshClientOptions.getTimeout());
			if (!completed)
				throw new IOException(String.format("connect timed out. hostname:%s port:%s timeoutMillis:%s",
						sshClientOptions.getHostname(), sshClientOptions.getPort(),
						sshClientOptions.getTimeout().toMillis()));
			clientSession = connect.getSession();
			if (sshClientOptions.getPassword() != null) {
				clientSession.addPasswordIdentity(sshClientOptions.getPassword());
				clientSession.auth().verify(sshClientOptions.getTimeout());
			} else if (sshClientOptions.getRsaPrivateKey() != null) {
				var rsaPrivateKey = sshClientOptions.getRsaPrivateKey();
				clientSession.addPublicKeyIdentity(
						new KeyPair(Utils.Crypto.generateRSAPublicKey(rsaPrivateKey), rsaPrivateKey));
				clientSession.auth().verify(sshClientOptions.getTimeout());
			}
			return SSHClientContext.builder().sshClient(sshClient).clientSession(clientSession).build();
		} catch (Exception e) {
			Utils.Exceptions.closeQuietly(Level.WARN, sshClient, clientSession);
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}
}
