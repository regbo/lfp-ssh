package com.lfp.ssh.impl.factory;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Objects;

import org.apache.sshd.common.forward.ForwarderFactory;
import org.apache.sshd.common.forward.Forwarder;
import org.apache.sshd.common.forward.ForwarderFactory;
import org.apache.sshd.common.forward.PortForwardingEventListener;
import org.apache.sshd.common.forward.PortForwardingEventListenerManager;
import org.apache.sshd.common.future.CloseFuture;
import org.apache.sshd.common.future.SshFutureListener;
import org.apache.sshd.common.session.ConnectionService;
import org.apache.sshd.common.util.net.SshdSocketAddress;
import org.apache.sshd.server.ServerBuilder;

public abstract class LocalPortForwardingRequestInterceptor implements ForwarderFactory {

	private final ForwarderFactory factoryDelegate;

	public LocalPortForwardingRequestInterceptor() {
		this(ServerBuilder.DEFAULT_FORWARDER_FACTORY);
	}

	public LocalPortForwardingRequestInterceptor(ForwarderFactory factoryDelegate) {
		this.factoryDelegate = Objects.requireNonNull(factoryDelegate);
	}

	@Override
	public Forwarder create(ConnectionService service) {
		var forwarder = factoryDelegate.create(service);
		if (forwarder == null)
			return null;
		return new Forwarder() {
			@Override
			public SshdSocketAddress localPortForwardingRequested(SshdSocketAddress local) throws IOException {
				return LocalPortForwardingRequestInterceptor.this.localPortForwardingRequested(service,
						forwarder, local);
			}

			// delegates

			@Override
			public Collection<PortForwardingEventListenerManager> getRegisteredManagers() {
				return forwarder.getRegisteredManagers();
			}

			@Override
			public SshdSocketAddress startLocalPortForwarding(SshdSocketAddress local, SshdSocketAddress remote)
					throws IOException {
				return forwarder.startLocalPortForwarding(local, remote);
			}

			@Override
			public NavigableSet<Integer> getStartedLocalPortForwards() {
				return forwarder.getStartedLocalPortForwards();
			}

			@Override
			public SshdSocketAddress getForwardedPort(int remotePort) {
				return forwarder.getForwardedPort(remotePort);
			}

			@Override
			public SshdSocketAddress getBoundLocalPortForward(int port) {
				return forwarder.getBoundLocalPortForward(port);
			}

			@Override
			public boolean addPortForwardingEventListenerManager(PortForwardingEventListenerManager manager) {
				return forwarder.addPortForwardingEventListenerManager(manager);
			}

			@Override
			public void addPortForwardingEventListener(PortForwardingEventListener listener) {
				forwarder.addPortForwardingEventListener(listener);
			}

			@Override
			public boolean removePortForwardingEventListenerManager(PortForwardingEventListenerManager manager) {
				return forwarder.removePortForwardingEventListenerManager(manager);
			}

			@Override
			public void stopLocalPortForwarding(SshdSocketAddress local) throws IOException {
				forwarder.stopLocalPortForwarding(local);
			}

			@Override
			public void removePortForwardingEventListener(PortForwardingEventListener listener) {
				forwarder.removePortForwardingEventListener(listener);
			}

			@Override
			public List<Entry<Integer, SshdSocketAddress>> getLocalForwardsBindings() {
				return forwarder.getLocalForwardsBindings();
			}

			@Override
			public SshdSocketAddress startRemotePortForwarding(SshdSocketAddress remote, SshdSocketAddress local)
					throws IOException {
				return forwarder.startRemotePortForwarding(remote, local);
			}

			@Override
			public CloseFuture close(boolean immediately) {
				return forwarder.close(immediately);
			}

			@Override
			public PortForwardingEventListener getPortForwardingEventListenerProxy() {
				return forwarder.getPortForwardingEventListenerProxy();
			}

			@Override
			public void localPortForwardingCancelled(SshdSocketAddress local) throws IOException {
				forwarder.localPortForwardingCancelled(local);
			}

			@Override
			public boolean isLocalPortForwardingStartedForPort(int port) {
				return forwarder.isLocalPortForwardingStartedForPort(port);
			}

			@Override
			public NavigableSet<Integer> getStartedRemotePortForwards() {
				return forwarder.getStartedRemotePortForwards();
			}

			@Override
			public void addCloseFutureListener(SshFutureListener<CloseFuture> listener) {
				forwarder.addCloseFutureListener(listener);
			}

			@Override
			public SshdSocketAddress getBoundRemotePortForward(int port) {
				return forwarder.getBoundRemotePortForward(port);
			}

			@Override
			public void removeCloseFutureListener(SshFutureListener<CloseFuture> listener) {
				forwarder.removeCloseFutureListener(listener);
			}

			@Override
			public List<Entry<Integer, SshdSocketAddress>> getRemoteForwardsBindings() {
				return forwarder.getRemoteForwardsBindings();
			}

			@Override
			public void stopRemotePortForwarding(SshdSocketAddress remote) throws IOException {
				forwarder.stopRemotePortForwarding(remote);
			}

			@Override
			public boolean isClosed() {
				return forwarder.isClosed();
			}

			@Override
			public boolean isRemotePortForwardingStartedForPort(int port) {
				return forwarder.isRemotePortForwardingStartedForPort(port);
			}

			@Override
			public SshdSocketAddress startDynamicPortForwarding(SshdSocketAddress local) throws IOException {
				return forwarder.startDynamicPortForwarding(local);
			}

			@Override
			public boolean isClosing() {
				return forwarder.isClosing();
			}

			@Override
			public boolean isOpen() {
				return forwarder.isOpen();
			}

			@Override
			public void stopDynamicPortForwarding(SshdSocketAddress local) throws IOException {
				forwarder.stopDynamicPortForwarding(local);
			}

			@Override
			public void close() throws IOException {
				forwarder.close();
			}
		};
	}

	protected abstract SshdSocketAddress localPortForwardingRequested(ConnectionService service,
			Forwarder forwarder, SshdSocketAddress local) throws IOException;
}
