package com.lfp.ssh.impl.filter;

import org.apache.sshd.common.session.Session;
import org.apache.sshd.common.util.net.SshdSocketAddress;
import org.apache.sshd.server.forward.ForwardingFilter;

public interface ForwardingFilterLFP extends ForwardingFilter {

	@Override
	default boolean canForwardAgent(Session session, String requestType) {
		return false;
	}

	@Override
	default boolean canForwardX11(Session session, String requestType) {
		return false;
	}

	@Override
	default boolean canListen(SshdSocketAddress address, Session session) {
		return false;
	}

	@Override
	default boolean canConnect(Type type, SshdSocketAddress address, Session session) {
		return false;
	}
	
	
}
